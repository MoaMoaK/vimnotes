" Debian-specific settings
runtime! debian.vim

set uc=0                            " Deactivate backups

let g:auto_save = 1                 " Enable Autosave
let g:auto_save_silent = 1          " Prevent autosave from echoing
let g:auto_save_in_insert_mode = 2  " Autosave even if insert mode is active
" Trigger the autosave on any change
let g:auto_save_events = ["InsertLeave", "TextChanged", "TextChangedI"]

" Remove bells
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Activate mouse (all modes)
set mouse=a

" Shortcuts for tabs
nnoremap <C-n> :tabnew<CR>:execute 'write ' . g:notes_dir . system('echo -n $RANDOM')<CR>i
nnoremap <C-w> :call delete(expand('%'))<CR>:tabclose!<CR>i
nnoremap <C-p> :tabnext<CR>i
nnoremap <C-o> :tabprevious<CR>i
nnoremap <C-s> :write <C-r>=expand("%:p:h")<CR>/
nnoremap <C-q> :quitall<CR>
noremap! <C-n> <ESC>:tabnew<CR>:execute 'write ' . g:notes_dir . system('echo -n $RANDOM')<CR>i
noremap! <C-w> <ESC>:call delete(expand('%'))<CR>:tabclose!<CR>i
noremap! <C-p> <ESC>:tabnext<CR>i
noremap! <C-o> <ESC>:tabprevious<CR>i
noremap! <C-s> <ESC>:write <C-r>=expand("%:p:h")<CR>/
noremap! <C-q> <ESC>:quitall<CR>

" Filename generation
let g:notes_dir = system('echo -n $NOTES_DIR')

" Set tab = 4 spaces by default
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

startinsert
