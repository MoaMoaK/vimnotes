# Vimnotes


## Synopsis

This little script is a home-made ultra simple note taking app.
The idea is to start vim in a directory and use each file in this directory as a note.
To make it more intuitive, vim is configured with custom commands.


## Motivation

I was looking for a very simple note-taking app, not a powerful note-taking apps like _Evernote_ but more like something I can quickly copy-paste something or just write down an idea. So I was not requiring any formatting (_markdown_, ...) of any sort.  
In addition I wanted something really easy to open and close so I am not slowed down by the time to open the app, choose a title/an existing app, ... In fact, the perfect app was an app I could open with a shortcut and immediatly start writing something and close with the same shortcut so I can get back to what I was doing.


## Setup

In my opinion, this script needs some setup to be really integrated and powerful.
I don't use it directly in a standard terminal.
I only open it in a drop-down terminal (and I'm only using it for that).

The purpose is that I only have to press a shortcut on my keyboard and a window with all my notes pops on top of everything (the terminal is configured with some transparency)


### Starting the script at startup

Depending on the terminal used the startup method may differ:

* If the terminal accepts a startup script (such as the gnome extension _drop down terminal_), it should be defined as `vimnotes.sh`.
* If the terminal accepts a startup shell (such as _guake_) it should be defined as `shell`. This is just a fake shell that start bash with the correct script.


### Defining a shortcut

The next step is to define your own shortcut to open and close the terminal. I personnaly like the _key above tab_ as I rarely use it and it is highly accessible.


## Features

* Stores all notes in a separated file under ~/.notes/
* Open all notes (under ~/.notes/) on startup in separate tabs in vim
* Use CTRL+O and CTRL+P to navigate between nodes
* Use CTRL+W to close a note and delete it (permanently)
* Use CTRL+S to save the note under a specific name or somewhere else
* Use CTRL+Q to save and close all notes and quit the program
* Use CTRL+N to open a new note (a random name is generated)
* The vim _AutoSave_ plugin is used to save the note everytime a vim buffer is updated
* The autosave features is trigerred whenever notes are created, saved or by goind to the next/previous notes
* Vim is always in INSERTION mode so it is easy to write things down (unless <ESC> is pressed)
