#!/bin/sh
NOTES_DIR=$HOME"/.notes/"
NOTES=$NOTES_DIR"*"
VIMRC=$(dirname $0)/vimrc

stty -ixon
NOTES_DIR=$NOTES_DIR /usr/bin/vim -u $VIMRC -p $NOTES
